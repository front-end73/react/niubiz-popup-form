import { FC, memo, ReactElement, useRef } from "react";
import useNiubizFormScript, {
  UseFormScriptProps,
} from "../../hooks/use-niubiz-form-script";
import { NiubizFormWithPrefix } from "../../types/niubiz";
import "./niubiz-popup-form.scss";

export type Props = NiubizFormWithPrefix & {
  handleOnLoad: () => void;
};

const NiubizPopupForm: FC<Props> = ({
  action,
  src,
  handleOnLoad,
  method,
  backgroundButton,
  ...rest
}: Props): ReactElement => {
  const form = useRef<HTMLFormElement>(null);

  const props: UseFormScriptProps = {
    src,
    async: true,
    checkForExisting: true,
    ...rest,
    form,
    onload: () => {
      handleOnLoad();
    },
  };

  useNiubizFormScript(props);

  return (
    <div className="niubiz-popup-form">
      <form ref={form} action={action} method={method} />
    </div>
  );
};

export default memo(NiubizPopupForm);
