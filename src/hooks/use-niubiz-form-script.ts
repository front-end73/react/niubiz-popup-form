import { useState, useEffect, RefObject } from "react";

const isBrowser =
  typeof window !== "undefined" && typeof window.document !== "undefined";

export type UseFormScriptProps = {
  src: HTMLScriptElement["src"] | null;
  checkForExisting?: boolean;
  form: RefObject<HTMLFormElement>;
  [key: string]: Any;
};

type ErrorState = ErrorEvent | null;

const useNiubizFormScript = ({
  src,
  checkForExisting = false,
  form,
  ...attributes
}: UseFormScriptProps): [boolean, ErrorState] => {
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<ErrorState>(null);

  useEffect(() => {
    if (!isBrowser || !src || !loading || error) return;

    if (checkForExisting) {
      const existing = document.querySelectorAll(`script[src="${src}"]`);
      if (existing.length > 0) {
        if (window.VisanetCheckout) delete window.VisanetCheckout;
        setLoading(false);
        return;
      }
    }

    const script = document.createElement("script");
    script.src = src;

    Object.keys(attributes).forEach((key) => {
      const sc = script as Any;
      if (sc[key] === undefined) {
        script.setAttribute(key, attributes[key]);
      } else {
        sc[key] = attributes[key];
      }
    });

    const handleLoad = () => {
      setLoading(false);
    };
    const handleError = (err: ErrorEvent) => {
      setError(err);
    };

    script.addEventListener("load", handleLoad);
    script.addEventListener("error", handleError);

    if (form && form.current) {
      if (window.VisanetCheckout) delete window.VisanetCheckout;
      form.current.appendChild(script);
    }

    return () => {
      script.removeEventListener("load", handleLoad);
      script.removeEventListener("error", handleError);
    };
  }, [attributes, checkForExisting, form, error, loading, src]);

  return [loading, error];
};
export default useNiubizFormScript;
