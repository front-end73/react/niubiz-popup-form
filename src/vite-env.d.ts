/// <reference types="vite/client" />

declare type Any = any;

type AddPrefixToObject<T, P extends string> = {
  [K in keyof T as K extends string ? `${P}${K}` : never]: T[K];
};

interface ImportMetaEnv {
  readonly VITE_APP_NIUBIZ_FORM_SRC: string;
  readonly VITE_APP_NIUBIZ_CHANNEL: string;
  readonly VITE_APP_NIUBIZ_MERCHANT_FORM_ACTION: string;
  readonly VITE_APP_NIUBIZ_MERCHANT_ID: string;
}

interface ImportMeta {
  readonly env: ImportMetaEnv;
}

declare interface Window {
  VisanetCheckout: Any;
}
