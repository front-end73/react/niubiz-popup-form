export const prefixObjectKeys = (object: Any, prefix = "data-") =>
  Object.keys(object).reduce((result: Any, key) => {
    result[`${prefix}${key}`] = object[key];
    return result;
  }, {});
