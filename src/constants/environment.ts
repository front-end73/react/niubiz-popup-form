export const environment: ImportMetaEnv = {
  VITE_APP_NIUBIZ_FORM_SRC: import.meta.env.VITE_APP_NIUBIZ_FORM_SRC,
  VITE_APP_NIUBIZ_CHANNEL: import.meta.env.VITE_APP_NIUBIZ_CHANNEL,
  VITE_APP_NIUBIZ_MERCHANT_ID: import.meta.env.VITE_APP_NIUBIZ_MERCHANT_ID,
  VITE_APP_NIUBIZ_MERCHANT_FORM_ACTION: import.meta.env
    .VITE_APP_NIUBIZ_MERCHANT_FORM_ACTION,
  BASE_URL: import.meta.env.BASE_URL,
  MODE: import.meta.env.MODE,
  DEV: import.meta.env.DEV,
  PROD: import.meta.env.PROD,
  SSR: import.meta.env.SSR,
};
