type NiubizRequiredProps = {
  merchantid: string;
  sessiontoken: string;
  amount: string;
  timeouturl: string;
  purchasenumber: string;
};

type NiubizOptionalProps = {
  channel?: string;
  expirationminutes?: string;
  merchantlogo?: string;
  merchantname?: string;
  buttonsize?: string;
  buttoncolor?: string;
  formbuttoncolor?: string;
  showamount?: string;
  cardholdername?: string;
  cardholderlastname?: string;
  cardholderemail?: string;
  usertoken?: string;
  hidexbutton?: string;
  formbuttontext?: string;
};

type NiubizRequiredWithoutPrefixProps = {
  action: string;
  src: string;
};

type NiubizOptionalWithoutPrefixProps = {
  method?: "POST" | "GET" | "PATCH" | "PUT";
  backgroundButton?: string;
};

type NiubizRequired = AddPrefixToObject<NiubizRequiredProps, "data-"> &
  NiubizRequiredWithoutPrefixProps;

type NiubizOptional = Partial<AddPrefixToObject<NiubizOptionalProps, "data-">> &
  NiubizOptionalWithoutPrefixProps;

export type NiubizFormWithPrefix = NiubizRequired & NiubizOptional;

export type NiubizForm = NiubizRequiredProps &
  NiubizOptionalProps &
  NiubizRequiredWithoutPrefixProps &
  NiubizOptionalWithoutPrefixProps;
