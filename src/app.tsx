import { FC, memo, ReactElement } from "react";
import HomePage from "./pages/home-page";

const App: FC = (): ReactElement => (
  <div className="app">
    <HomePage />
  </div>
);

export default memo(App);
