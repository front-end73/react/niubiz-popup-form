import { FC, memo, ReactElement } from "react";
import { environment } from "../constants/environment";
import { NiubizForm } from "../types/niubiz";
import { prefixObjectKeys } from "../utils/object";
import NiubizPopupForm from "../components/niubiz-popup-form/niubiz-popup-form";
import "./home-page.scss";

const HomePage: FC = (): ReactElement => {
  const handleOnLoad = (): void => {
    console.log("Loaded :>> ");
  };
  const props: NiubizForm = {
    src: environment.VITE_APP_NIUBIZ_FORM_SRC,
    channel: environment.VITE_APP_NIUBIZ_CHANNEL,
    formbuttoncolor: "#E3350D",
    expirationminutes: "20",
    timeouturl: "about:blank",
    merchantlogo: `${window.location.href}/images/pokemon.svg`,
    backgroundButton: `${window.location.href}/images/payment-button.png`,
    amount: "10.5",
    sessiontoken:
      "41e64fbbb00b4faf54fb6daf0988207f92c7c6c510c85338e0becf347b97f033",
    merchantid: environment.VITE_APP_NIUBIZ_MERCHANT_ID,
    purchasenumber: "2020100901",
    cardholdername: "Homero",
    cardholderlastname: "Simpson",
    formbuttontext: "Pagar",
    action: `${environment.VITE_APP_NIUBIZ_MERCHANT_FORM_ACTION}?applicationCode=1&jobId=123,channelName=BCP`,
    method: "POST",
  };

  const { action, src, method, ...rest } = props;

  const propsWithPrefix = {
    ...prefixObjectKeys(rest),
    action,
    src,
    method,
  };
  return (
    <div className="home-page">
      <NiubizPopupForm handleOnLoad={handleOnLoad} {...propsWithPrefix} />
    </div>
  );
};

export default memo(HomePage);
